Junit Basics
============

# Pre-requisites

- Java 17 or newer (preferred Java 21)
- Intellij IDEA Ultimate or Community Edition

# Cloning project with Git

- Clone or download the repository
- Adjust `Java` version in `pom.xml` (default: `17`)
- Run `./mvnw clean test` to build the project (`mvnw.cmd clean test` on `Windows`)
- Import the project to IntelliJ IDEA (`File` > `Open`)

# Importing project to IntelliJ IDEA

- Open `IntelliJ IDEA`
- Select `File` > `New` > `Project from Version Control`
- Type url of the repository: `https://gitlab.com/codeleak/workshops/merito/java-mvn-starter`
- Type directory where the project will be cloned
- Click `Clone`
