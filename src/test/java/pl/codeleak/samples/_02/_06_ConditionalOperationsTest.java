package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Classifier {

    // TODO: Implement a method that classifies an integer as negative, positive, or zero.
    static String classifyNumber(int num) {
        return "";
    }

    // TODO: Implement a method to determine if a number is even or odd.
    static String determineEvenOrOdd(int num) {
        return "";
    }

    // TODO: Implement a method that grades a score: 90-100 is 'A', 80-89 is 'B', 70-79 is 'C', 60-69 is 'D', below 60 is 'F'.
    static char gradeScore(int score) {
        return ' ';
    }
}

class _06_ConditionalOperationsTest {

    @Test
    public void testClassifyNumber() {
        // TODO: assert that -5 is classified as "negative"
        // TODO: assert that 5 is classified as "positive"
        // TODO: assert that 0 is classified as "zero"
    }

    @Test
    public void testDetermineEvenOrOdd() {
        // TODO: assert that 4 is "even"
        // TODO: assert that 5 is "odd"
    }

    @Test
    public void testGradeScore() {
        // TODO: assert that a score of 95 returns 'A'
        // TODO: assert that a score of 85 returns 'B'
        // TODO: assert that a score of 75 returns 'C'
        // TODO: assert that a score of 65 returns 'D'
        // TODO: assert that a score of 55 returns 'F'
    }
}
