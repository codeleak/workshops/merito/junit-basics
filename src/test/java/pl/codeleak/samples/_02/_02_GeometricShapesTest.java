package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// TODO: Implement the Rectangle class with a method to calculate the area.
class Rectangle {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double area() {
        // TODO Placeholder implementation. Update this!
        return 0.0;
    }
}

// TODO: Implement the Circle class with a method to calculate the area.
class Circle {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double area() {
        // TODO Placeholder implementation. Update this!
        return 0.0;
    }
}

public class _02_GeometricShapesTest {

    @Test
    public void testRectangleArea() {
        Rectangle rectangle = new Rectangle(4, 7);
        // TODO: assert that area of rectangle with length 4 and width 7 should be 28
    }

    @Test
    public void testCircleArea() {
        Circle circle = new Circle(5);
        // TODO: assert that area of circle with radius 5 should be approx. 78.54
    }

    // TODO Optional
    // Implement perimeter calculations for each shape.
    // Include other geometric shapes like triangles, squares, etc.
}
