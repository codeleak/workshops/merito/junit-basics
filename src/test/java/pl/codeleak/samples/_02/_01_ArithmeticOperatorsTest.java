package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

class Calculator {

    // TODO: Implement the method to get the remainder after division.
    static int getModulus(int a, int b) {
        return 0;
    }

    // TODO: Implement the method to increment a number.
    static int increment(int a) {
        return 0;
    }

    // TODO: Implement the method to decrement a number.
    static int decrement(int a) {
        return 0;
    }
}

public class _01_ArithmeticOperatorsTest {

    @Test
    public void testModulus() {
        // TODO: assert that 5 % 2 should equal 1
    }

    @Test
    public void testIncrement() {
        // TODO: assert that incrementing 5 should give 6
    }

    @Test
    public void testDecrement() {
        // TODO: assert that decrementing 5 should give 4
    }
}
