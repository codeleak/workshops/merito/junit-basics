package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringOperations {

    // TODO: Implement a method to concatenate two strings.
    static String concatenateStrings(String a, String b) {
        return "";
    }

    // TODO: Implement a method to determine the length of a string.
    static int getStringLength(String str) {
        return 0;
    }

    // TODO: Implement a method to get a substring from a given string.
    static String getSubstring(String str, int start, int end) {
        return "";
    }

    // TODO: Implement a method to get a character from a specific position in a string.
    static char getCharacterAtPosition(String str, int position) {
        return ' ';
    }
}

public class _03_StringOperationsTest {

    @Test
    public void testConcatenateStrings() {
        // TODO: assert that concatenating "Hello" and " World" results in "Hello World"
    }

    @Test
    public void testGetStringLength() {
        // TODO: assert that the length of the string "Hello" is 5
    }

    @Test
    public void testGetSubstring() {
        // TODO: assert that the substring of "Hello World" from position 0 to 5 is "Hello"
    }

    @Test
    public void testGetCharacterAtPosition() {
        // TODO: assert that the character at position 4 of the string "Hello" is 'o'
    }
}
