package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayOperations {

    // TODO: Implement a method to find the maximum value in an array.
    static int findMaxValue(int[] arr) {
        return 0;
    }

    // TODO: Implement a method to find the minimum value in an array.
    static int findMinValue(int[] arr) {
        return 0;
    }

    // TODO: Implement a method to calculate the average of values in an array.
    static double calculateAverage(int[] arr) {
        return 0.0;
    }

    // TODO: Implement a method to reverse an array.
    static int[] reverseArray(int[] arr) {
        return new int[0];
    }
}

public class _05_ArrayOperationsTest {

    @Test
    public void testFindMaxValue() {
        // TODO: assert that the maximum value in the array [1, 3, 5, 2, 4] is 5
    }

    @Test
    public void testFindMinValue() {
        // TODO: assert that the minimum value in the array [1, 3, 5, 2, 4] is 1
    }

    @Test
    public void testCalculateAverage() {
        // TODO: assert that the average of values in the array [1, 2, 3, 4, 5] is 3.0
    }

    @Test
    public void testReverseArray() {
        // TODO: assert that reversing the array [1, 2, 3, 4, 5] results in [5, 4, 3, 2, 1]
    }
}

