package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class _07_ObjectEqualityTest {

    @Test
    public void testBookReferenceEquality() {
        Book bookA = new Book("Java Basics", "John Doe", "12345");
        Book bookB = bookA;

        // TODO: assert that bookA and bookB refer to the same object in memory
    }

    @Test
    public void testBookValueEquality() {
        Book bookA = new Book("Java Basics", "John Doe", "12345");
        Book bookB = new Book("Java Basics", "John Doe", "12345");

        // TODO: assert that bookA and bookB are equal based on their ISBNs (use the overridden equals method)
    }

    @Test
    public void testBookInequality() {
        Book bookA = new Book("Java Basics", "John Doe", "12345");
        Book bookB = new Book("Advanced Java", "Jane Smith", "67890");

        // TODO: assert that bookA and bookB are not equal based on their ISBNs
    }
}

class Book {
    String title;
    String author;
    String ISBN;

    Book(String title, String author, String ISBN) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
    }

    // TODO: Override the equals method to compare based on the ISBN.
    @Override
    public boolean equals(Object obj) {
        // Placeholder implementation. Update this!
        return super.equals(obj);
    }
}
