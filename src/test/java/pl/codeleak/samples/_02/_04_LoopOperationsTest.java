package pl.codeleak.samples._02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoreStringOperations {

    // TODO: Implement a method to repeat a string N times using a for loop.
    static String repeatStringForLoop(String str, int times) {
        return "";
    }

    // TODO: Implement a method to count the occurrences of a char in a string using a while loop.
    static int countCharOccurrencesWhileLoop(String str, char c) {
        return 0;
    }

    // TODO: Implement a method to reverse a string using a do-while loop.
    static String reverseStringDoWhileLoop(String str) {
        return "";
    }
}

class _04_LoopOperationsTest {


    @Test
    public void testRepeatStringForLoop() {
        // TODO: assert that repeating the string "ab" 3 times results in "ababab"
    }

    @Test
    public void testCountCharOccurrencesWhileLoop() {
        // TODO: assert that the number of 'l's in the string "Hello World" is 3
    }

    @Test
    public void testReverseStringDoWhileLoop() {
        // TODO: assert that reversing the string "abcd" results in "dcba"
    }
}
