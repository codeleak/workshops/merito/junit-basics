package pl.codeleak.samples._05;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;


class _01_OrderServiceTest {
    private OrderService orderService;
    private ProductRepository productRepositoryMock;

    @BeforeEach
    void setUp() {
        // TODO: Mock the interface
        productRepositoryMock = null; // Placeholder
        // TODO: Create an instance of the class under test
        orderService = null; // Placeholder
    }

    @Test
    void testCalculateTotalOrderValue() {
        List<Product> products = List.of(
                new Product("Laptop", 1000.0),
                new Product("Mouse", 50.0),
                new Product("Keyboard", 60.0)
        );
        // TODO: Define the behavior of the mock: when the fetchAllProducts() method is called on the mock, return a list of products
        when(null);

        // Call the method under test
        double totalValue = orderService.calculateTotalOrderValue();

        // TODO: Verify that the fetchAllProducts() method was called on the mock and assert the result
        verify(null); // Placeholder

        // Assert the result
        assertEquals(1110.0, totalValue);
    }
}

class Product {

    private final String name;
    private final double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

// Note that there's no concrete implementation available
interface ProductRepository {
    List<Product> fetchAllProducts();
}

class OrderService {

    private final ProductRepository productRepository;

    public OrderService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public double calculateTotalOrderValue() {
        return productRepository.fetchAllProducts()
                .stream()
                .mapToDouble(Product::getPrice)
                .sum();
    }
}

