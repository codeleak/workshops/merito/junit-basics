package pl.codeleak.samples._05;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class _02_NumbersProcessorTest {

    private NumbersProcessor numbersProcessor;
    private NumbersGenerator numbersGeneratorMock;

    @BeforeEach
    void setUp() {
        numbersProcessor = new NumbersProcessor();

        // TODO: Create a mock of the NumbersGenerator interface
        numbersGeneratorMock = null; // Placeholder
    }

    @Test
    void processAndSumEvenNumbers() {
        // TODO: Define the behavior of the mock: when the numbers() method is called on the mock, return a list of numbers 1 to 5
        // Tip: To create a list of numbers 1 to 5, use List.of(1, 2, 3, 4, 5)
        when(null); // Placeholder

        int result = numbersProcessor.processAndSumEvenNumbers(numbersGeneratorMock);

        // TODO: Verify numbers() method was called on the mock
        verify(null); // Placeholder

        // Assert result
        assertEquals(6, result);
    }
}

interface NumbersGenerator {
    List<Integer> numbers();
}

class NumbersProcessor {
    public int processAndSumEvenNumbers(NumbersGenerator numbersGenerator) {
        return numbersGenerator.numbers()
                .stream()
                .filter(num -> num % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum();
    }
}
