package pl.codeleak.samples._03;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class _04_ParameterizedValueSource {

    @ParameterizedTest
    // TODO: Provide 5 even numbers as parameters
    @ValueSource(ints = {})
    void testEvenNumbers(int number) {
        // TODO: Implement logic to check if the number is even
    }

    @ParameterizedTest
    // TODO: Provide 5 strings with a length greater than 3 as parameters
    @ValueSource(strings = {"apple", "banana", "cherry", "date"})
    void testStringLengthGreaterThanThree(String fruit) {
        // TODO: Implement logic to check if given strings have a length greater than 3
    }

    // TODO: Create a new parameterized test that checks if given numbers are positive
}
