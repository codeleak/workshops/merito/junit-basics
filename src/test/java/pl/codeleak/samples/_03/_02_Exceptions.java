package pl.codeleak.samples._03;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankAccount {
    private double balance;

    public BankAccount(double balance) {
        this.balance = balance;
    }

    public void withdraw(double amount) throws InsufficientFundsException {
        // TODO: Throw an InsufficientFundsException if the amount is greater than the balance

        balance -= amount;
    }
}

class InsufficientFundsException extends Exception {
    public InsufficientFundsException(String message) {
        super(message);
    }
}

class _02_Exceptions {

    @Test
    public void testWithdrawalExceedingBalance() {
        BankAccount account = new BankAccount(100); // Initial balance is 100

        // TODO: Use assertThrows to expect an InsufficientFundsException when trying to withdraw more than the balance
        Exception exception = assertThrows(InsufficientFundsException.class, () -> {
            account.withdraw(150); // Trying to withdraw 150
        });

        // TODO: Check the exception message to ensure it's accurate
    }
}
