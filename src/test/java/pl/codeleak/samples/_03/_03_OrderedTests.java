package pl.codeleak.samples._03;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

// TODO: Change the test order to ordered
@TestMethodOrder(MethodOrderer.Random.class)
class _03_OrderedTests {

    private static boolean firstTestPassed = false;
    private static boolean secondTestPassed = false;

    @Test
    // TODO: Change the test order to 1
    public void firstTest() {
        // TODO: Implement some logic here.
        firstTestPassed = true;
        assertTrue(firstTestPassed);
    }

    @Test
    // TODO: Change the test order to 2
    public void secondTest() {
        // TODO: This test assumes that the first test has been executed. Implement some logic to validate this.
        assertTrue(firstTestPassed);
        secondTestPassed = true;
    }

    @Test
    // TODO: Change the test order to 3
    public void thirdTest() {
        // TODO: This test assumes that the first and second tests have been executed. Implement logic to validate this.
        assertTrue(firstTestPassed && secondTestPassed);
    }
}
