package pl.codeleak.samples._03;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureConverter {

    public double celsiusToFahrenheit(double celsius) throws InvalidTemperatureException {
        // TODO: Throw an InvalidTemperatureException if the temperature is below absolute zero
        // Tip: Absolute zero is -273.15 Celsius

        return celsius * 9 / 5 + 32;
    }
}

class InvalidTemperatureException extends Exception {
    public InvalidTemperatureException(String message) {
        super(message);
    }
}

class _01_Fail {

    @Test
    public void testCelsiusToFahrenheitConversion() {
        TemperatureConverter converter = new TemperatureConverter();

        double result = 0;
        try {
            result = converter.celsiusToFahrenheit(0);
            // TODO: Assert that the conversion of 0°C is 32°F

        } catch (InvalidTemperatureException e) {
            fail("Exception should not be thrown for valid temperature values.");
        }
    }

    @Test
    public void testInvalidTemperature() {
        TemperatureConverter converter = new TemperatureConverter();

        try {
            converter.celsiusToFahrenheit(-300); // Below absolute zero
            fail("Expected an InvalidTemperatureException to be thrown");
        } catch (InvalidTemperatureException e) {
            // TODO: Check the exception message to ensure it's accurate
        }
    }
}
