package pl.codeleak.samples._01;

import org.junit.jupiter.api.*;

class Calculator {
    int add(int a, int b) {
        return a + b;
    }

    int subtract(int a, int b) {
        return a - b;
    }
}

class _01_Lifecycle {

    Calculator calculator;

    @BeforeAll
    static void setupOnce() {
        // TODO: Print a message indicating this runs once before all tests.
    }

    @BeforeEach
    void setup() {
        // TODO: initialize the calculator with a predefined state and observe how it affects each test.
        // TODO: Print a message indicating a new Calculator object has been created.
    }

    @Test
    public void testAddition() {
        // TODO: Test the addition method of the Calculator.
    }

    @Test
    public void testSubtraction() {
        // TODO: Test the subtraction method of the Calculator.
    }

    @AfterEach
    void tearDown() {
        calculator = null;
        // TODO: Print a message indicating the Calculator object has been set to null/cleaned up.
    }

    @AfterAll
    static void tearDownOnce() {
        // TODO: Print a message indicating this runs once after all tests.
    }


}
