package pl.codeleak.samples._04;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class _01_AssertJBasics {

    @Test
    public void stringManipulationTest() {
        // Given the word
        String word = "AssertJ";

        // TODO: Use AssertJ to check if the word starts with "Asser", ends with "J" and contains "sert" using AssertJ
    }

    @Test
    public void listContainsTest() {
        // Given a list of fruits
        String[] fruits = {"apple", "banana", "cherry", "date"};

        // TODO: Use AssertJ to check if the list contains "banana" and does not contain "grape"
    }

    @Test
    public void numberComparisonTest() {
        // Given the number
        int number = 5;

        // TODO: Use AssertJ to check if the number is greater than 3 and less than 7
    }

    @Test
    public void objectPropertiesTest() {
        // Given the person
        Person person = new Person("John", 25);

        // TODO: Use AssertJ to check if the person's name is "John" and age is 25
        // Tip: Use hasFieldOrPropertyWithValue method
    }

    @Test
    public void mapEntryTest() {
        // Given a map of fruit calories
        Map<String, Integer> fruitCalories = new HashMap<>();
        fruitCalories.put("apple", 95);
        fruitCalories.put("banana", 105);

        // TODO: Use AssertJ to check if the map contains an entry with key "apple" and value 95 and does not contain an entry with key "orange"
    }

    @Test
    public void exceptionAssertionTest() {
        // Given a null string
        String word = null;

        // TODO: Use AssertJ to check if a NullPointerException is thrown when calling .length() on a null string
        // Tip: Use assertThatExceptionOfType method
    }

    class Person {
        String name;
        int age;

        Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}
